public class Main {

    private int amount;
    private int possibleDept;
    private boolean frozen;
    private int accountNumber;

    public Main(){
        frozen = false;
        amount = 0;
        possibleDept = 0;
        accountNumber = 0;
    }

    public int getAmount(){
        return amount;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public int getPossibleDept(){
        return this.possibleDept;
    }

    public void setPossibleDept(int possibleDept){
        this.possibleDept = possibleDept;
    }

    public boolean withdraw(int amount){
        if(this.frozen){
            return false;
        }
        if(this.amount + possibleDept > amount){
            this.amount -= amount;
            return true;
        }
        return false;
    }

    public void freeze(){
        this.frozen = true;
    }

    public void unfreeze(){
        this.frozen = false;
    }

    public boolean deposit(int amount){
        if(this.frozen){
            return false;
        }
        this.amount += amount;
        return true;
    }

    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("Account number: " + accountNumber + "\n");
        sb.append("Amount: " + amount + "\n");
        sb.append("Possible dept: " + possibleDept + "\n");
        if(frozen){
            sb.append("Account is frozen");
        }else{
            sb.append("Account is active");
        }
        return sb.toString();
    }
}
